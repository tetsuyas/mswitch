class ManifestosController < ApplicationController
  before_action :set_manifesto, only: [:show, :edit, :update, :destroy]

  # GET /manifestos
  # GET /manifestos.json
  def index
    @manifestos = Manifesto.all
  end

  # GET /manifestos/1
  # GET /manifestos/1.json
  def show
  end

  # GET /manifestos/new
  def new
    @manifesto = Manifesto.new
  end

  # GET /manifestos/1/edit
  def edit
  end

  # POST /manifestos
  # POST /manifestos.json
  def create
    @manifesto = Manifesto.new(manifesto_params)

    respond_to do |format|
      if @manifesto.save
        format.html { redirect_to @manifesto, notice: 'Manifesto was successfully created.' }
        format.json { render :show, status: :created, location: @manifesto }
      else
        format.html { render :new }
        format.json { render json: @manifesto.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /manifestos/1
  # PATCH/PUT /manifestos/1.json
  def update
    respond_to do |format|
      if @manifesto.update(manifesto_params)
        format.html { redirect_to @manifesto, notice: 'Manifesto was successfully updated.' }
        format.json { render :show, status: :ok, location: @manifesto }
      else
        format.html { render :edit }
        format.json { render json: @manifesto.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /manifestos/1
  # DELETE /manifestos/1.json
  def destroy
    @manifesto.destroy
    respond_to do |format|
      format.html { redirect_to manifestos_url, notice: 'Manifesto was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_manifesto
      @manifesto = Manifesto.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def manifesto_params
      params.require(:manifesto).permit(:manifesto_type_id, :is_open, :name, :name_kana, :pref_id, :area, :reason, :vision, :theme, :mypolicy1field1, :mypolicy1field2, :mypolicy1field3, :mypolicy1content, :mypolicy2field1, :mypolicy2field2, :mypolicy2field3, :mypolicy2content, :mypolicy3field1, :mypolicy3field2, :mypolicy3field3, :mypolicy3content, :policyfield1, :policyfield2, :policyfield3, :policyfield4, :policyfield5, :policyfield6, :policyfield7, :policyfield8, :policyfield9, :policyfield10, :kensyo, :file_nae, :acccept, :senkyoku, :senkyoname, :topic1_1cont, :topic1_1term, :topic1_1goal, :topic1_1reso, :topic1_2cont, :topic1_2term, :topic1_2goal, :topic1_2reso, :topic1_3cont, :topic1_3term, :topic1_3goal, :topic1_3reso, :topic2_1cont, :topic2_1term, :topic2_1goal, :topic2_1reso, :topic2_2cont, :topic2_2term, :topic2_2goal, :topic2_2reso, :topic2_3cont, :topic2_3term, :topic2_3goal, :topic2_3reso, :topic3_1cont, :topic3_1term, :topic3_1goal, :topic3_1reso, :topic3_2cont, :topic3_2term, :topic3_2goal, :topic3_2reso, :topic3_3cont, :topic3_3term, :topic3_3goal, :topic3_3reso, :assem_act1, :assem_act2, :assem_act3, :url_, :photo, :geo, :policyfieldsum)
    end
end
