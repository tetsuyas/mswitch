class SeijikasController < ApplicationController
  before_action :set_seijika, only: [:show, :edit, :update, :destroy]

  # GET /seijikas
  # GET /seijikas.json
  def index
    @seijikas = Seijika.all
  end

  # GET /seijikas/1
  # GET /seijikas/1.json
  def show
  end

  # GET /seijikas/new
  def new
    @seijika = Seijika.new
  end

  # GET /seijikas/1/edit
  def edit
  end

  # POST /seijikas
  # POST /seijikas.json
  def create
    @seijika = Seijika.new(seijika_params)

    respond_to do |format|
      if @seijika.save
        format.html { redirect_to @seijika, notice: 'Seijika was successfully created.' }
        format.json { render :show, status: :created, location: @seijika }
      else
        format.html { render :new }
        format.json { render json: @seijika.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /seijikas/1
  # PATCH/PUT /seijikas/1.json
  def update
    respond_to do |format|
      if @seijika.update(seijika_params)
        format.html { redirect_to @seijika, notice: 'Seijika was successfully updated.' }
        format.json { render :show, status: :ok, location: @seijika }
      else
        format.html { render :edit }
        format.json { render json: @seijika.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /seijikas/1
  # DELETE /seijikas/1.json
  def destroy
    @seijika.destroy
    respond_to do |format|
      format.html { redirect_to seijikas_url, notice: 'Seijika was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_seijika
      @seijika = Seijika.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def seijika_params
      params.require(:seijika).permit(:user_type, :sei, :mei, :seikana, :meikana, :sei_roma, :mei_roma, :mail1, :mail2, :birthday, :tel, :password, :pref, :sex_, :photo, :website_url, :blog_url, :twitter, :facebook, :movie, :certification, :certificated_at, :city, :office_zip, :office_pref, :office_city, :office_address, :office_building, :office_tel, :ofice_mobile)
    end
end
