class Manifesto < ActiveRecord::Base
  belongs_to :manifesto_type
  belongs_to :pref
  belongs_to :mypolicy1field1, class_name:"SeisakuBunya",foreign_key:"mypolicy1field1_id", primary_key:"id"
  belongs_to :mypolicy1field2, class_name:"SeisakuBunya",foreign_key:"mypolicy1field2_id", primary_key:"id"
  belongs_to :mypolicy1field3, class_name:"SeisakuBunya",foreign_key:"mypolicy1field3_id", primary_key:"id"
  belongs_to :mypolicy2field1, class_name:"SeisakuBunya",foreign_key:"mypolicy2field1_id", primary_key:"id"
  belongs_to :mypolicy2field2, class_name:"SeisakuBunya",foreign_key:"mypolicy2field2_id", primary_key:"id"
  belongs_to :mypolicy2field3, class_name:"SeisakuBunya",foreign_key:"mypolicy2field3_id", primary_key:"id"
  belongs_to :mypolicy3field1, class_name:"SeisakuBunya",foreign_key:"mypolicy3field1_id", primary_key:"id"
  belongs_to :mypolicy3field2, class_name:"SeisakuBunya",foreign_key:"mypolicy3field2_id", primary_key:"id"
  belongs_to :mypolicy3field3, class_name:"SeisakuBunya",foreign_key:"mypolicy3field3_id", primary_key:"id"
end
