class CreateManifestoTypes < ActiveRecord::Migration
  def change
    create_table :manifesto_types do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
