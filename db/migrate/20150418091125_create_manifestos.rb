class CreateManifestos < ActiveRecord::Migration
  def change
    create_table :manifestos do |t|
      t.references :manifesto_type, index: true, foreign_key: true
      t.boolean :is_open
      t.string :name
      t.string :name_kana
      t.references :pref, index: true, foreign_key: true
      t.string :area
      t.text :reason
      t.text :vision
      t.text :theme
      t.integer :mypolicy1field1_id
      t.integer :mypolicy1field2_id
      t.integer :mypolicy1field3_id
      t.text :mypolicy1content
      t.integer :mypolicy2field1_id
      t.integer :mypolicy2field2_id
      t.integer :mypolicy2field3_id
      t.text :mypolicy2content
      t.integer :mypolicy3field1_id
      t.integer :mypolicy3field2_id
      t.integer :mypolicy3field3_id
      t.text :mypolicy3content
      t.integer :policyfield1
      t.integer :policyfield2
      t.integer :policyfield3
      t.integer :policyfield4
      t.integer :policyfield5
      t.integer :policyfield6
      t.integer :policyfield7
      t.integer :policyfield8
      t.integer :policyfield9
      t.integer :policyfield10
      t.boolean :kensyo
      t.text :file_name
      t.boolean :acccept
      t.text :senkyoku
      t.text :senkyoname
      t.text :topic1_1cont
      t.text :topic1_1term
      t.text :topic1_1goal
      t.text :topic1_1reso
      t.text :topic1_2cont
      t.text :topic1_2term
      t.text :topic1_2goal
      t.text :topic1_2reso
      t.text :topic1_3cont
      t.text :topic1_3term
      t.text :topic1_3goal
      t.text :topic1_3reso
      t.text :topic2_1cont
      t.text :topic2_1term
      t.text :topic2_1goal
      t.text :topic2_1reso
      t.text :topic2_2cont
      t.text :topic2_2term
      t.text :topic2_2goal
      t.text :topic2_2reso
      t.text :topic2_3cont
      t.text :topic2_3term
      t.text :topic2_3goal
      t.text :topic2_3reso
      t.text :topic3_1cont
      t.text :topic3_1term
      t.text :topic3_1goal
      t.text :topic3_1reso
      t.text :topic3_2cont
      t.text :topic3_2term
      t.text :topic3_2goal
      t.text :topic3_2reso
      t.text :topic3_3cont
      t.text :topic3_3term
      t.text :topic3_3goal
      t.text :topic3_3reso
      t.text :assem_act1
      t.text :assem_act2
      t.text :assem_act3
      t.text :url_
      t.binary :photo
      t.text :geo
      t.integer :policyfieldsum

      t.timestamps null: false
    end
  end
end
