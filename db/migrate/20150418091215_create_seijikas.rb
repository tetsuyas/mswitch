class CreateSeijikas < ActiveRecord::Migration
  def change
    create_table :seijikas do |t|
      t.integer :user_type
      t.string :sei
      t.string :mei
      t.string :sei_kana
      t.string :mei_kana
      t.string :sei_roma
      t.string :mei_roma
      t.string :mail1
      t.string :mail2
      t.datetime :birthday
      t.string :tel
      t.string :password
      t.string :pref
      t.boolean :sex_
      t.binary :photo
      t.text :website_url
      t.text :blog_url
      t.text :twitter
      t.text :facebook
      t.text :movie
      t.boolean :certification
      t.datetime :certificated_at
      t.text :city
      t.string :office_zip
      t.string :office_pref
      t.text :office_city
      t.text :office_address
      t.text :office_building
      t.text :office_tel
      t.text :office_mobile

      t.timestamps null:false
    end
  end
end
