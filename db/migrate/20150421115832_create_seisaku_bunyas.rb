class CreateSeisakuBunyas < ActiveRecord::Migration
  def change
    create_table :seisaku_bunyas do |t|
      t.string :name

      t.timestamps null:false
    end
  end
end
