# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150421115832) do

  create_table "manifesto_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "manifestos", force: :cascade do |t|
    t.integer  "manifesto_type_id",  limit: 4
    t.boolean  "is_open",            limit: 1
    t.string   "name",               limit: 255
    t.string   "name_kana",          limit: 255
    t.integer  "pref_id",            limit: 4
    t.string   "area",               limit: 255
    t.text     "reason",             limit: 65535
    t.text     "vision",             limit: 65535
    t.text     "theme",              limit: 65535
    t.integer  "mypolicy1field1_id", limit: 4
    t.integer  "mypolicy1field2_id", limit: 4
    t.integer  "mypolicy1field3_id", limit: 4
    t.text     "mypolicy1content",   limit: 65535
    t.integer  "mypolicy2field1_id", limit: 4
    t.integer  "mypolicy2field2_id", limit: 4
    t.integer  "mypolicy2field3_id", limit: 4
    t.text     "mypolicy2content",   limit: 65535
    t.integer  "mypolicy3field1_id", limit: 4
    t.integer  "mypolicy3field2_id", limit: 4
    t.integer  "mypolicy3field3_id", limit: 4
    t.text     "mypolicy3content",   limit: 65535
    t.integer  "policyfield1",       limit: 4
    t.integer  "policyfield2",       limit: 4
    t.integer  "policyfield3",       limit: 4
    t.integer  "policyfield4",       limit: 4
    t.integer  "policyfield5",       limit: 4
    t.integer  "policyfield6",       limit: 4
    t.integer  "policyfield7",       limit: 4
    t.integer  "policyfield8",       limit: 4
    t.integer  "policyfield9",       limit: 4
    t.integer  "policyfield10",      limit: 4
    t.boolean  "kensyo",             limit: 1
    t.text     "file_name",          limit: 65535
    t.boolean  "acccept",            limit: 1
    t.text     "senkyoku",           limit: 65535
    t.text     "senkyoname",         limit: 65535
    t.text     "topic1_1cont",       limit: 65535
    t.text     "topic1_1term",       limit: 65535
    t.text     "topic1_1goal",       limit: 65535
    t.text     "topic1_1reso",       limit: 65535
    t.text     "topic1_2cont",       limit: 65535
    t.text     "topic1_2term",       limit: 65535
    t.text     "topic1_2goal",       limit: 65535
    t.text     "topic1_2reso",       limit: 65535
    t.text     "topic1_3cont",       limit: 65535
    t.text     "topic1_3term",       limit: 65535
    t.text     "topic1_3goal",       limit: 65535
    t.text     "topic1_3reso",       limit: 65535
    t.text     "topic2_1cont",       limit: 65535
    t.text     "topic2_1term",       limit: 65535
    t.text     "topic2_1goal",       limit: 65535
    t.text     "topic2_1reso",       limit: 65535
    t.text     "topic2_2cont",       limit: 65535
    t.text     "topic2_2term",       limit: 65535
    t.text     "topic2_2goal",       limit: 65535
    t.text     "topic2_2reso",       limit: 65535
    t.text     "topic2_3cont",       limit: 65535
    t.text     "topic2_3term",       limit: 65535
    t.text     "topic2_3goal",       limit: 65535
    t.text     "topic2_3reso",       limit: 65535
    t.text     "topic3_1cont",       limit: 65535
    t.text     "topic3_1term",       limit: 65535
    t.text     "topic3_1goal",       limit: 65535
    t.text     "topic3_1reso",       limit: 65535
    t.text     "topic3_2cont",       limit: 65535
    t.text     "topic3_2term",       limit: 65535
    t.text     "topic3_2goal",       limit: 65535
    t.text     "topic3_2reso",       limit: 65535
    t.text     "topic3_3cont",       limit: 65535
    t.text     "topic3_3term",       limit: 65535
    t.text     "topic3_3goal",       limit: 65535
    t.text     "topic3_3reso",       limit: 65535
    t.text     "assem_act1",         limit: 65535
    t.text     "assem_act2",         limit: 65535
    t.text     "assem_act3",         limit: 65535
    t.text     "url_",               limit: 65535
    t.binary   "photo",              limit: 65535
    t.text     "geo",                limit: 65535
    t.integer  "policyfieldsum",     limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "manifestos", ["manifesto_type_id"], name: "index_manifestos_on_manifesto_type_id", using: :btree
  add_index "manifestos", ["pref_id"], name: "index_manifestos_on_pref_id", using: :btree

  create_table "prefs", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "seijikas", force: :cascade do |t|
    t.integer  "user_type",       limit: 4
    t.string   "sei",             limit: 255
    t.string   "mei",             limit: 255
    t.string   "sei_kana",        limit: 255
    t.string   "mei_kana",        limit: 255
    t.string   "sei_roma",        limit: 255
    t.string   "mei_roma",        limit: 255
    t.string   "mail1",           limit: 255
    t.string   "mail2",           limit: 255
    t.datetime "birthday"
    t.string   "tel",             limit: 255
    t.string   "password",        limit: 255
    t.string   "pref",            limit: 255
    t.boolean  "sex_",            limit: 1
    t.binary   "photo",           limit: 65535
    t.text     "website_url",     limit: 65535
    t.text     "blog_url",        limit: 65535
    t.text     "twitter",         limit: 65535
    t.text     "facebook",        limit: 65535
    t.text     "movie",           limit: 65535
    t.boolean  "certification",   limit: 1
    t.datetime "certificated_at"
    t.text     "city",            limit: 65535
    t.string   "office_zip",      limit: 255
    t.string   "office_pref",     limit: 255
    t.text     "office_city",     limit: 65535
    t.text     "office_address",  limit: 65535
    t.text     "office_building", limit: 65535
    t.text     "office_tel",      limit: 65535
    t.text     "office_mobile",   limit: 65535
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "seisaku_bunyas", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_foreign_key "manifestos", "manifesto_types"
  add_foreign_key "manifestos", "prefs"
end
