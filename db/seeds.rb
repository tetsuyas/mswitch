# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


open(Rails.root.join("db/pref.txt")) {|file|
  while l = file.gets
    Pref.find_or_create_by(name:l.chomp())
  end
}

open(Rails.root.join("db/manifesto_types.txt")){|file|
  while l = file.gets
    ManifestoType.find_or_create_by(name:l.chomp())
  end
}
puts "Create ManifestoType count = #{ManifestoType.all.count}"

open(Rails.root.join("db/seisaku_bunyas.txt")){|file|
  while l = file.gets
    SeisakuBunya.find_or_create_by(name:l.split("\t")[1].chomp())
  end
}

puts "Create SeisakuBunya count = #{SeisakuBunya.all.count}"