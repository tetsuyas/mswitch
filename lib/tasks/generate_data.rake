namespace :generate_data do
  desc "generate manifesto"
  task :init => :environment do
    Rake::Task["db:migrate:reset"].invoke()
    p "DONE Rake::Task['db:migrate:reset']"
    Rake::Task["db:seed"].invoke()
    p "DONE Rake::Task['db:seed']"
    Rake::Task["generate_data:manifesto"].invoke()
    p "DONE Rake::Task['generate_data:manifesto']"
  end

  task :manifesto => :environment do
    open(Rails.root.join("db/manifesto_headless_tsv.txt")) {|file|
      while l = file.gets
        a = l.split("\t")
        mt = ManifestoType.where(name:a[3].chomp()).first
        pref = Pref.where(name:a[6]).first
        Manifesto.create({
         manifesto_type: mt,
         name:a[4],
         name_kana:a[5],
         pref: pref,
         area:a[7],
         reason:a[9],
         vision:a[10],
         theme:a[11],
         mypolicy1field1: (SeisakuBunya.where(name:a[12]).try(:first)),
         mypolicy1field2: (SeisakuBunya.where(name:a[13]).try(:first)),
         mypolicy1field3: (SeisakuBunya.where(name:a[14]).try(:first)),
         mypolicy1content:a[15],
         mypolicy2field1: (SeisakuBunya.where(name:a[16]).try(:first)),
         mypolicy2field2: (SeisakuBunya.where(name:a[17]).try(:first)),
         mypolicy2field3: (SeisakuBunya.where(name:a[18]).try(:first)),
         mypolicy2content:a[19],
         mypolicy3field1: (SeisakuBunya.where(name:a[20]).try(:first)),
         mypolicy3field2: (SeisakuBunya.where(name:a[21]).try(:first)),
         mypolicy3field3: (SeisakuBunya.where(name:a[22]).try(:first)),
         mypolicy3content:a[23],
         policyfield1:a[24].to_i,
         policyfield2:a[25].to_i,
         policyfield3:a[26].to_i,
         policyfield4:a[27].to_i,
         policyfield5:a[28].to_i,
         policyfield6:a[29].to_i,
         policyfield7:a[30].to_i,
         policyfield8:a[31].to_i,
         policyfield9:a[31].to_i,
         policyfield10:a[32].to_i,
         url_:a[35] })
      end
    }
  end
end




=begin
         kensyo
         file_name
         acccept
         senkyoku
         senkyoname
         topic1_1cont
         topic1_1term
         topic1_1goal
         topic1_1reso
         topic1_2cont
         topic1_2term
         topic1_2goal
         topic1_2reso
         topic1_3cont
         topic1_3term
         topic1_3goal
         topic1_3reso
         topic2_1cont
         topic2_1term
         topic2_1goal
         topic2_1reso
         topic2_2cont
         topic2_2term
         topic2_2goal
         topic2_2reso
         topic2_3cont
         topic2_3term
         topic2_3goal
         topic2_3reso
         topic3_1cont
         topic3_1term
         topic3_1goal
         topic3_1reso
         topic3_2cont
         topic3_2term
         topic3_2goal
         topic3_2reso
         topic3_3cont
         topic3_3term
         topic3_3goal
         topic3_3reso
         assem_act1
         assem_act2
         assem_act3
         url_:a[35]
         photo
         geo
         policyfieldsum
=end
=begin
0 マニフェストID
1 登録日時
2 更新日時
3 マニフェスト種別: manifestotype
4 政治家名: name
5 政治家名カナ表記: name:kana
6 対象の都道府県             pref
  対象の市区町村または都道府県  area
8 対象の選挙区
9 政治家を志した理由  reason
10 地域のありたい姿    vision
11解決したい課題     theme
12重要政策1政策分野1 mypolicyfield1
13重要政策1政策分野2 mypolicyfield2
14重要政策1政策分野3 mypolicyfield3
15解決するための重要政策1説明
重要政策2政策分野1
重要政策2政策分野2
重要政策2政策分野3
19解決するための重要政策2説明
重要政策3政策分野1
重要政策3政策分野2
重要政策3政策分野3
23解決するための重要政策3説明
政策分野注力度1社会保障
政策分野注力度2産業政策
政策分野注力度3社会資本整備
政策分野注力度4教育・子育て
政策分野注力度5農林漁業
政策分野注力度6税財政・財政再建
政策分野注力度7労働
政策分野注力度8環境・エネルギー
政策分野注力度9行政・議会改革
32政策分野注力度10安全・防災・震災復興
検証
オープンデータ化（報道等の利活用）に関する許諾
政策の詳細へのURL
=end



