require 'test_helper'

class ManifestosControllerTest < ActionController::TestCase
  setup do
    @manifesto = manifestos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:manifestos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create manifesto" do
    assert_difference('Manifesto.count') do
      post :create, manifesto: { acccept: @manifesto.acccept, area: @manifesto.area, assem_act1: @manifesto.assem_act1, assem_act2: @manifesto.assem_act2, assem_act3: @manifesto.assem_act3, file_nae: @manifesto.file_nae, geo: @manifesto.geo, is_open: @manifesto.is_open, kensyo: @manifesto.kensyo, manifesto_type_id: @manifesto.manifesto_type_id, mypolicy1content: @manifesto.mypolicy1content, mypolicy1field1: @manifesto.mypolicy1field1, mypolicy1field2: @manifesto.mypolicy1field2, mypolicy1field3: @manifesto.mypolicy1field3, mypolicy2content: @manifesto.mypolicy2content, mypolicy2field1: @manifesto.mypolicy2field1, mypolicy2field2: @manifesto.mypolicy2field2, mypolicy2field3: @manifesto.mypolicy2field3, mypolicy3content: @manifesto.mypolicy3content, mypolicy3field1: @manifesto.mypolicy3field1, mypolicy3field2: @manifesto.mypolicy3field2, mypolicy3field3: @manifesto.mypolicy3field3, name: @manifesto.name, name_kana: @manifesto.name_kana, photo: @manifesto.photo, policyfield10: @manifesto.policyfield10, policyfield1: @manifesto.policyfield1, policyfield2: @manifesto.policyfield2, policyfield3: @manifesto.policyfield3, policyfield4: @manifesto.policyfield4, policyfield5: @manifesto.policyfield5, policyfield6: @manifesto.policyfield6, policyfield7: @manifesto.policyfield7, policyfield8: @manifesto.policyfield8, policyfield9: @manifesto.policyfield9, policyfieldsum: @manifesto.policyfieldsum, pref_id: @manifesto.pref_id, reason: @manifesto.reason, senkyoku: @manifesto.senkyoku, senkyoname: @manifesto.senkyoname, theme: @manifesto.theme, topic1_1cont: @manifesto.topic1_1cont, topic1_1goal: @manifesto.topic1_1goal, topic1_1reso: @manifesto.topic1_1reso, topic1_1term: @manifesto.topic1_1term, topic1_2cont: @manifesto.topic1_2cont, topic1_2goal: @manifesto.topic1_2goal, topic1_2reso: @manifesto.topic1_2reso, topic1_2term: @manifesto.topic1_2term, topic1_3cont: @manifesto.topic1_3cont, topic1_3goal: @manifesto.topic1_3goal, topic1_3reso: @manifesto.topic1_3reso, topic1_3term: @manifesto.topic1_3term, topic2_1cont: @manifesto.topic2_1cont, topic2_1goal: @manifesto.topic2_1goal, topic2_1reso: @manifesto.topic2_1reso, topic2_1term: @manifesto.topic2_1term, topic2_2cont: @manifesto.topic2_2cont, topic2_2goal: @manifesto.topic2_2goal, topic2_2reso: @manifesto.topic2_2reso, topic2_2term: @manifesto.topic2_2term, topic2_3cont: @manifesto.topic2_3cont, topic2_3goal: @manifesto.topic2_3goal, topic2_3reso: @manifesto.topic2_3reso, topic2_3term: @manifesto.topic2_3term, topic3_1cont: @manifesto.topic3_1cont, topic3_1goal: @manifesto.topic3_1goal, topic3_1reso: @manifesto.topic3_1reso, topic3_1term: @manifesto.topic3_1term, topic3_2cont: @manifesto.topic3_2cont, topic3_2goal: @manifesto.topic3_2goal, topic3_2reso: @manifesto.topic3_2reso, topic3_2term: @manifesto.topic3_2term, topic3_3cont: @manifesto.topic3_3cont, topic3_3goal: @manifesto.topic3_3goal, topic3_3reso: @manifesto.topic3_3reso, topic3_3term: @manifesto.topic3_3term, url_: @manifesto.url_, vision: @manifesto.vision }
    end

    assert_redirected_to manifesto_path(assigns(:manifesto))
  end

  test "should show manifesto" do
    get :show, id: @manifesto
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @manifesto
    assert_response :success
  end

  test "should update manifesto" do
    patch :update, id: @manifesto, manifesto: { acccept: @manifesto.acccept, area: @manifesto.area, assem_act1: @manifesto.assem_act1, assem_act2: @manifesto.assem_act2, assem_act3: @manifesto.assem_act3, file_nae: @manifesto.file_nae, geo: @manifesto.geo, is_open: @manifesto.is_open, kensyo: @manifesto.kensyo, manifesto_type_id: @manifesto.manifesto_type_id, mypolicy1content: @manifesto.mypolicy1content, mypolicy1field1: @manifesto.mypolicy1field1, mypolicy1field2: @manifesto.mypolicy1field2, mypolicy1field3: @manifesto.mypolicy1field3, mypolicy2content: @manifesto.mypolicy2content, mypolicy2field1: @manifesto.mypolicy2field1, mypolicy2field2: @manifesto.mypolicy2field2, mypolicy2field3: @manifesto.mypolicy2field3, mypolicy3content: @manifesto.mypolicy3content, mypolicy3field1: @manifesto.mypolicy3field1, mypolicy3field2: @manifesto.mypolicy3field2, mypolicy3field3: @manifesto.mypolicy3field3, name: @manifesto.name, name_kana: @manifesto.name_kana, photo: @manifesto.photo, policyfield10: @manifesto.policyfield10, policyfield1: @manifesto.policyfield1, policyfield2: @manifesto.policyfield2, policyfield3: @manifesto.policyfield3, policyfield4: @manifesto.policyfield4, policyfield5: @manifesto.policyfield5, policyfield6: @manifesto.policyfield6, policyfield7: @manifesto.policyfield7, policyfield8: @manifesto.policyfield8, policyfield9: @manifesto.policyfield9, policyfieldsum: @manifesto.policyfieldsum, pref_id: @manifesto.pref_id, reason: @manifesto.reason, senkyoku: @manifesto.senkyoku, senkyoname: @manifesto.senkyoname, theme: @manifesto.theme, topic1_1cont: @manifesto.topic1_1cont, topic1_1goal: @manifesto.topic1_1goal, topic1_1reso: @manifesto.topic1_1reso, topic1_1term: @manifesto.topic1_1term, topic1_2cont: @manifesto.topic1_2cont, topic1_2goal: @manifesto.topic1_2goal, topic1_2reso: @manifesto.topic1_2reso, topic1_2term: @manifesto.topic1_2term, topic1_3cont: @manifesto.topic1_3cont, topic1_3goal: @manifesto.topic1_3goal, topic1_3reso: @manifesto.topic1_3reso, topic1_3term: @manifesto.topic1_3term, topic2_1cont: @manifesto.topic2_1cont, topic2_1goal: @manifesto.topic2_1goal, topic2_1reso: @manifesto.topic2_1reso, topic2_1term: @manifesto.topic2_1term, topic2_2cont: @manifesto.topic2_2cont, topic2_2goal: @manifesto.topic2_2goal, topic2_2reso: @manifesto.topic2_2reso, topic2_2term: @manifesto.topic2_2term, topic2_3cont: @manifesto.topic2_3cont, topic2_3goal: @manifesto.topic2_3goal, topic2_3reso: @manifesto.topic2_3reso, topic2_3term: @manifesto.topic2_3term, topic3_1cont: @manifesto.topic3_1cont, topic3_1goal: @manifesto.topic3_1goal, topic3_1reso: @manifesto.topic3_1reso, topic3_1term: @manifesto.topic3_1term, topic3_2cont: @manifesto.topic3_2cont, topic3_2goal: @manifesto.topic3_2goal, topic3_2reso: @manifesto.topic3_2reso, topic3_2term: @manifesto.topic3_2term, topic3_3cont: @manifesto.topic3_3cont, topic3_3goal: @manifesto.topic3_3goal, topic3_3reso: @manifesto.topic3_3reso, topic3_3term: @manifesto.topic3_3term, url_: @manifesto.url_, vision: @manifesto.vision }
    assert_redirected_to manifesto_path(assigns(:manifesto))
  end

  test "should destroy manifesto" do
    assert_difference('Manifesto.count', -1) do
      delete :destroy, id: @manifesto
    end

    assert_redirected_to manifestos_path
  end
end
