require 'test_helper'

class SeijikasControllerTest < ActionController::TestCase
  setup do
    @seijika = seijikas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:seijikas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create seijika" do
    assert_difference('Seijika.count') do
      post :create, seijika: { birthday: @seijika.birthday, blog_url: @seijika.blog_url, certificated_at: @seijika.certificated_at, certification: @seijika.certification, city: @seijika.city, facebook: @seijika.facebook, mail1: @seijika.mail1, mail2: @seijika.mail2, mei: @seijika.mei, mei_roma: @seijika.mei_roma, meikana: @seijika.meikana, movie: @seijika.movie, office_address: @seijika.office_address, office_building: @seijika.office_building, office_city: @seijika.office_city, office_pref: @seijika.office_pref, office_tel: @seijika.office_tel, office_zip: @seijika.office_zip, ofice_mobile: @seijika.ofice_mobile, password: @seijika.password, photo: @seijika.photo, pref: @seijika.pref, sei: @seijika.sei, sei_roma: @seijika.sei_roma, seikana: @seijika.seikana, sex_: @seijika.sex_, tel: @seijika.tel, twitter: @seijika.twitter, user_type: @seijika.user_type, website_url: @seijika.website_url }
    end

    assert_redirected_to seijika_path(assigns(:seijika))
  end

  test "should show seijika" do
    get :show, id: @seijika
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @seijika
    assert_response :success
  end

  test "should update seijika" do
    patch :update, id: @seijika, seijika: { birthday: @seijika.birthday, blog_url: @seijika.blog_url, certificated_at: @seijika.certificated_at, certification: @seijika.certification, city: @seijika.city, facebook: @seijika.facebook, mail1: @seijika.mail1, mail2: @seijika.mail2, mei: @seijika.mei, mei_roma: @seijika.mei_roma, meikana: @seijika.meikana, movie: @seijika.movie, office_address: @seijika.office_address, office_building: @seijika.office_building, office_city: @seijika.office_city, office_pref: @seijika.office_pref, office_tel: @seijika.office_tel, office_zip: @seijika.office_zip, ofice_mobile: @seijika.ofice_mobile, password: @seijika.password, photo: @seijika.photo, pref: @seijika.pref, sei: @seijika.sei, sei_roma: @seijika.sei_roma, seikana: @seijika.seikana, sex_: @seijika.sex_, tel: @seijika.tel, twitter: @seijika.twitter, user_type: @seijika.user_type, website_url: @seijika.website_url }
    assert_redirected_to seijika_path(assigns(:seijika))
  end

  test "should destroy seijika" do
    assert_difference('Seijika.count', -1) do
      delete :destroy, id: @seijika
    end

    assert_redirected_to seijikas_path
  end
end
